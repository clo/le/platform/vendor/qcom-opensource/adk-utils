/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#define ADK_LOG_BACKEND ADK_LOG_BACKEND_STDOUT
#define ADK_LOG_LEVEL adk_log_level::DEBUG
#include "adk/log.h"
#include <unistd.h>

int main() {
  int i = 0;

  ADK_LOG_EMERG("this is an emergency, i = %d", i++);
  ADK_LOG_ALERT("this is an alert, i = %d", i++);
  ADK_LOG_CRIT("this is critical, i = %d", i++);
  ADK_LOG_ERROR("this is an error, i = %d", i++);
  ADK_LOG_WARNING("this is a warning, i = %d", i++);
  ADK_LOG_NOTICE("this is a notice, i = %d", i++);
  ADK_LOG_INFO("this is info, i = %d", i++);
  ADK_LOG_DEBUG("this is debug, i = %d", i++);

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_STDOUT
  ADK_LOG_EMERG("stdout backend");
#endif

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_STDERR
  ADK_LOG_EMERG("stderr backend");
#endif

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_SYSLOG
  ADK_LOG_EMERG("syslog backend");
#endif

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_SYSTEMD
  ADK_LOG_EMERG("systemd backend",
                "MESSAGE_ID=%d", 1234, "MANAGER=button");
#endif

  // If the application is being run by systemd AND the systemd backend is
  // selected, journald needs to find the application which sent the log entry 
  // in the systemd service listing to be able to insert the _SYSTEMD_UNIT 
  // field in to the log. Therefore, we need to delay the return for a few 
  // seconds. Minimum time is unknown.
  sleep(2);

  return 0;
}

