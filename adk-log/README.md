# adk-log
Logging library for ADK

## Docs
To build the docs:

~~~
mkdir build
cd build
cmake ..
make
~~~

Point browser at `build/htmldocs/html/index.html`

## Prequisites

libsystemd-journal - required for ADK_LOG_BACKEND_SYSTEMD
```
sudo apt-get install libsystemd-journal-dev
```

## Build
Backends stdout, stderr (default), and syslog should just build. The systemd backend requires `$(pkg-config --libs libsystemd-journal)` on the build command-line.

## Example Applications

All that is needed is to include the header file, and use the ADK_LOG() macro.

Minimal:
~~~
#include "adk/log.h"

int main() {
  ADK_LOG_DEBUG("Hello World! My name is %s", "Astrid" );
  return 0;
}
~~~

More examples:
~~~
#define ADK_LOG_BACKEND ADK_LOG_BACKEND_SYSTEMD
#define ADK_LOG_LEVEL adk_log_level::DEBUG
#include "adk/log.h"
#include <unistd.h>

int main() {
  int i = 0;

  ADK_LOG_EMERG("this is an emergency, i = %d", i++);
  ADK_LOG_ALERT("this is an alert, i = %d", i++);
  ADK_LOG_CRIT("this is critical, i = %d", i++);
  ADK_LOG_ERROR("this is an error, i = %d", i++);
  ADK_LOG_WARNING("this is a warning, i = %d", i++);
  ADK_LOG_NOTICE("this is a notice, i = %d", i++);
  ADK_LOG_INFO("this is info, i = %d", i++);
  ADK_LOG_DEBUG("this is debug, i = %d", i++);

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_STDOUT
  ADK_LOG_EMERG("stdout backend");
#endif

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_STDERR
  ADK_LOG_EMERG("stderr backend");
#endif

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_SYSLOG
  ADK_LOG_EMERG("syslog backend");
#endif

#if ADK_LOG_BACKEND == ADK_LOG_BACKEND_SYSTEMD
  ADK_LOG_EMERG("systemd backend",
                "MESSAGE_ID=%d", 1234, "MANAGER=button");
#endif

  // If the application is being run by systemd AND the systemd backend is
  // selected, journald needs to find the application which sent the log entry 
  // in the systemd service listing to be able to insert the _SYSTEMD_UNIT 
  // field in to the log. Therefore, we need to delay the return for a few 
  // seconds. Minimum time is unknown.
  sleep(2);

  return 0;
}
~~~

## Release Notes:
06 July 2018
* small fixes for building correctly on Yocto and Jenkins
* systemd backend not currently supported in the build

02 July 2018
* changed name/path to adk/log.h

28th June 2018
* default backend is now STDERR
* moved to adk-utils
* cmake installer works
* backend selection changed
* fixed test application for systemd backend, now works as expected

12th June 2018
* early release for evaluation, started in personal github
* logging works
* backends supported: stdout, stderr, syslog, systemd
* CMakeLists.txt will only build the Doxygen docs. There is no make or install target.
* test directory contains a simple test example
